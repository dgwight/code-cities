const fs = require('fs')
const axios = require('axios')
const cheerio = require('cheerio')
const pThrottle = require('p-throttle')

const cities = require('./input')
const minRepos = 5
const throttledUserCount = pThrottle(getUserCount, 1, 15000)

Promise.all(cities.map(city => sumCity(city))).then(cities => {
  const current = require('./input')
  fs.writeFile('./output.json', JSON.stringify(current.push(city)), 'utf8', _ => {})
})

function sumCity (city) {
  return Promise.all(Object.keys(city.search_names).map(key => {
    return throttledUserCount(key, city.search_names[key], minRepos)
  })).then(counts => {
    // Sum user counts for each city search term i.e. ["New York", "NYC"]
    const totalDevelopers = counts.reduce((acc, val) => acc + val)
    city.developers = totalDevelopers
    console.log(city)
    const output = require('./output')
    fs.writeFile('./output.json', JSON.stringify(output.push(city)), 'utf8', _ => {})
    return city
  })
}

function getUserCount (city, isAdded, minRepos) {
  const url = `https://github.com/search?p=2&q=repos%3A%3E=${minRepos}+location%3A%22${city}%22&type=Users`
  return axios.get(url).then(response => {
    // Scrape user count from page
    const $ = cheerio.load(response.data)
    const selector = '#js-pjax-container > div > div.columns > div.column.three-fourths.codesearch-results > div > div.d-flex.flex-justify-between.border-bottom.pb-3 > h3'
    const count = parseInt(($(selector).text()).replace(/,/g, '').replace(/\n/g, '').replace(/users/g, ''))
    return isAdded ? count : count * -1
  }).catch(err => {
    console.error(err.response.status, err.response.statusText, err.config.url)
    return 0
  })
}
