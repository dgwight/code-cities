// https://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  env: {
    node: true,
    browsers: true
  },
  extends: 'standard'
}
